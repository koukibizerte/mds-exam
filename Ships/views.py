from django.shortcuts import render
import pandas as pd
from django import forms


#############
#Specific form: column with which the table is sorted
#############
class SortForm(forms.Form):
    def __init__(self, *args, **kwargs):
        myChoices = kwargs.pop("mychoices")
        super(SortForm, self).__init__(*args, **kwargs)
        self.fields['sortmethod'] = forms.ChoiceField(label='Sort by', required=False, choices=myChoices,
                                                      widget=forms.Select(attrs={'onchange': 'this.form.submit();'}))


#########
#Calling the index page
#load the csv file in a pandas dataframe
#generate a form depending on the request variable
#returning a render to the index template with a context
#########
def index(request):
    vessels = pd.read_csv('Ships/vessels.csv', sep='|', index_col=False)
    columns = [''.join(column.split()) for column in vessels.columns]
    vessels.columns = columns
    choices = [(column, column) for column in columns]
    if request.method == 'POST':
        form = SortForm(request.POST, mychoices=choices)
        if form.is_valid():
            sorted_column = form.cleaned_data['sortmethod']
            vessels = vessels.sort(sorted_column)
            vessels.index = range(len(vessels[sorted_column]))
    else:
        form = SortForm(mychoices=[('', '...')] + choices)
    context = {'vessels': vessels.to_json(), 'form': form}
    return render(request, 'Ships/index.htm', context)
