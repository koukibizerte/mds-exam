### MDS Exam solution ###

This is a Django web application proposed as a solution for the [MDS Exam](https://github.com/shekoff/com.mds.exam).


The Django framework generates automatically some folders and files needed in the deployment and development of the project.


The must important files are : 

- ships/views.py contains the python code.

- ships/vessels.csv

- template/index.htm contains the template with some python injected variables.

To run this application, Django library 1.8.2 is required, and the wsgi.py file needed  for the deployement is located in the folder "MdsExam"


Note: to facilitate the execution of the code, I recommend the use of an IDE which support Django projects like Pycharm.